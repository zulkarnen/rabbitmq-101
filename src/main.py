from amqpstorm import Connection, Message



with Connection('localhost', 'guest', 'guest') as connection :
    with connection.channel() as channel :

        channel.queue.declare('fruits')

        message = Message.create(channel, "Hello RabbitMQ", {'content_type': 'text/plain', 'headers': {'key': 'value'} })
        
        message.publish('fruits')


import logging

from amqpstorm import Connection
from amqpstorm import Message

logging.basicConfig(level=logging.INFO)

with Connection('localhost', 'guest', 'guest') as connection:
    with connection.channel() as channel:
        # Declare the Queue, 'simple_queue'.
        channel.queue.declare('simple_queue')

        # Message Properties.
        properties = {
            'content_type': 'text/plain',
            'headers': {'key': 'value'}
        }

        # Create the message.
        message = Message.create(channel, 'Hello Dunia!', properties)

        # Publish the message to a queue called, 'simple_queue'.
        message.publish('simple_queue')